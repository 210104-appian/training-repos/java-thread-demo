package dev.rehm.gc;

public class MyObject {
	
	static int count;
	
	@Override 
	public void finalize() {
		count++;
		System.out.println(count+ ": garbage collected");
	}
	
	
	public static void main(String[] args) {
		
		for(int i=0; i<100; i++) {
			MyObject o = new MyObject();
			System.out.println(o);
		}
		System.gc(); // this suggests to my JVM to run garbage collection
		

	}

}
