package dev.rehm.sync;

public class Counter {
	
	volatile int value;
	
	public synchronized void increment() {
		value++;
	}

}
