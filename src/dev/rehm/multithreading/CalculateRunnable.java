package dev.rehm.multithreading;

public class CalculateRunnable implements Runnable {
	
	int factor = 0;
	
	public CalculateRunnable() {
		super();
	}
	
	public CalculateRunnable(int factor) {
		super();
		this.factor = factor;
	}

	@Override
	public void run() {
		for(int i =0 ; i<factor; i++) {
			executeTask();
		}
		
	}
	
	public void executeTask() {
		int value = 100;
		for(int i=0; i<1000;i++) {
			value*=(Math.random()*100);
			for(int j=0; j<1000; j++) {
				value/=(Math.random()*100);
			}
		}
	}

}
