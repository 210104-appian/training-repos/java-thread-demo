package dev.rehm.multithreading;

public class Driver {

	public static void main(String[] args) {
		final int NUM_OF_TASKS = 12;
		final int NUM_OF_THREADS = 6;


		Runnable job = new CalculateRunnable(NUM_OF_TASKS/NUM_OF_THREADS);
		
		Thread t1 = new Thread(job);
		Thread t2 = new Thread(job);
		Thread t3 = new Thread(job);
		Thread t4 = new Thread(job);
		Thread t5 = new Thread(job);
		Thread t6 = new Thread(job);


		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();
		t6.start();
		
			
	}


}
